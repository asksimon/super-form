import React, { Component } from 'react';
import { connect } from 'react-redux';

import Input from '@ui/Input';
import Button from '@ui/Button';
import Form from '@ui/Form';
import { getData, setData } from '@storageApi';
import { setData as setDataActionCreator } from '@actions/bankCard';

class BankCard extends Component {
  state = {
    numberCard: '',
    error: {}
  };

  componentDidMount() {
    const bankCard = this.props.bankCard;

    if (bankCard) {
      this.setState({
        ...this.state,
        ...bankCard,
      })
    } else {
      getData('bankCard').then(data => {
        this.setState({
          ...this.state,
          ...data,
        })
      }).then(() => {
        this.props.dispatch(setDataActionCreator(this.state))
      })
    }
  }

  handleChangeNumberCard = e => {
    this.setState({
      numberCard: e.target.value.replace(/\s/g, '')
    })
  };

  rules = {
    numberCard: {
      required: true,
      pattern: /^\d{16}$/
    }
  };

  cbError = (error) => {
    this.setState({
      error: {
        ...this.state.error,
        ...error,
      }
    })
  };

  cbSubmit = () => {
    this.setState({
      error: {}
    }, () => {
      setData('bankCard', this.state).then(() => {
        this.props.dispatch(setDataActionCreator(this.state));
      }).then(() => {
        this.props.addCountCompleteForm('bankCard');
      }).then(() => {
        this.props.goNextForm();
      })
    })
  };

  render() {
    const formatNumber = this.state.numberCard.match(/.{1,4}/g);
    const cardNumber = formatNumber ? formatNumber.join(' ') : '';

    return (
      <Form
        rules={this.rules}
        values={this.state}
        cbError={this.cbError}
        cbSubmit={this.cbSubmit}
      >
        <Input
          value={cardNumber}
          onChange={this.handleChangeNumberCard}
          error={this.state.error.numberCard}
          label="Номер банковской карты"
          id="bank-card__number-card"
        />

        <Button onClick={() => {
          this.props.goPrevForm();
        }} type="button">Назад</Button>

        <Button>Далее</Button>
      </Form>
    )
  }
}

export default connect(
  state => ({
    bankCard: state.bankCard
  })
)(BankCard);
