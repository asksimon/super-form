import React, { Component } from 'react';
import { connect } from 'react-redux';

import Input from '@ui/Input';
import Checkbox from '@ui/Checkbox';
import Button from '@ui/Button';
import Form from '@ui/Form';
import { getData, setData } from '@storageApi';
import { setData as setDataActionCreator } from '@actions/personalData';

class PersonalData extends Component {
  state = {
    name: '',
    age: '',
    confirmAge: false,
    error: {},
  };

  componentDidMount() {
    const personalData = this.props.personalData;

    if (personalData) {
      this.setState({
        ...this.state,
        ...personalData,
      })
    } else {
      getData('personalData').then(data => {

        this.setState({
          ...this.state,
          ...data,
        })
      }).then(() => {
        this.props.dispatch(setDataActionCreator(this.state))
      })
    }
  }

  handleChangeName = e => {
    this.setState({
      name: e.target.value
    })
  };

  handleChangeAge = e => {
    this.setState({
      age: e.target.value
    })
  };

  handleChangeConfirmAge = e => {
    this.setState({
      confirmAge: e.target.checked
    })
  };

  rules = {
    name: {
      required: true,
      pattern: /^[A-Za-z]+$/
    },
    age: {
      required: true,
      pattern: /^\d+$/
    },
    confirmAge: {
      isChecked: true,
    }
  };

  cbError = (error) => {
    this.setState({
      error: {
        ...this.state.error,
        ...error,
      }
    })
  };

  cbSubmit = () => {
    this.setState({
      error: {},
    }, () => {
      setData('personalData', this.state).then(() => {
        this.props.dispatch(setDataActionCreator(this.state));
      }).then(() => {
        this.props.addCountCompleteForm('personalData');
      }).then(() => {
        this.props.goNextForm();
      })
    })
  };

  render() {
    return (
      <Form
        rules={this.rules}
        values={this.state}
        cbError={this.cbError}
        cbSubmit={this.cbSubmit}
      >
        <Input
          value={this.state.name}
          onChange={this.handleChangeName}
          error={this.state.error.name}
          label="Имя"
          id="personal-data__name"
        />
        <Input
          value={this.state.age}
          label="Возраст"
          error={this.state.error.age}
          onChange={this.handleChangeAge}
          id="personal-data__age"
        />
        <Checkbox
          onChange={this.handleChangeConfirmAge}
          label="Мне есть 18 лет"
          checked={this.state.confirmAge}
          error={this.state.error.confirmAge}
        />
        <Button>Далее</Button>
      </Form>
    )
  }
}

export default connect(
  state => ({
    personalData: state.personalData
  })
)(PersonalData);
