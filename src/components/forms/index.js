import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';

import { getData, setData } from '@storageApi';

class Form extends Component {
  state = {
    BankCard: null,
    Completion: null,
    PersonalData: null,
    completeForm: [],
  };

  getComponent = (componentName, props) => {
    if (!this.state[componentName]) {
      import(`./${componentName}`).then((module) => {
        this.setState({
          [componentName]: createElement(module.default, props)
        })
      }).catch(error => {
        console.error('Не удалось загрузить чанк!');
      })
    }
  };

  componentDidMount() {
    getData('completeForm').then(data => {
      this.setState({
        completeForm: data ||  [],
      })
    });
  }

  addCountCompleteForm = (formName) => {
    if (this.state.completeForm.indexOf(formName) === -1) {
      this.state.completeForm.push(formName);

      this.setState({
        completeForm: [...this.state.completeForm]
      }, () => {
        setData('completeForm', this.state.completeForm)
      })
    }
  };

  render() {
    const {currentForm, goPrevForm, goNextForm} = this.context;

    this.getComponent(currentForm, {
      goPrevForm,
      goNextForm,
      addCountCompleteForm: this.addCountCompleteForm,
      removeCountCompleteForm: this.removeCountCompleteForm,
    });

    return (
      <div style={{width: '500px', margin: '0 auto'}}>
        <h3>Количество заполненых форм: {this.state.completeForm.length}</h3>
        {
          this.state[currentForm] ?
            this.state[currentForm] :
            <div>...LOADER</div>
        }
      </div>
    )
  }
}

Form.contextTypes = {
  currentForm: PropTypes.string,
  goPrevForm: PropTypes.func,
  goNextForm: PropTypes.func,
};

export default Form;
