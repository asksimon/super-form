import React, { Component } from 'react';

import Button from '@ui/Button';

class Completion extends Component {
  state = {
    isDone: false,
  };

  handleClickBtn = () => {
    this.setState({isDone: true})
  };

  render() {
    return (
      <div>
        {
          this.state.isDone ?
            <span>Завершено!</span> :
            <div>
              <Button onClick={() => {
                this.props.goPrevForm();
              }} type="button">Назад</Button>
              <Button onClick={this.handleClickBtn} type="button">Далее</Button>
            </div>
        }
      </div>
    )
  }
}

export default Completion;
