import React from 'react';

const Input = props => {
  const {
    id,
    label,
    className,
    error,
    ...nativeProps,
  } = props;

  return (
    <div className={`form-group ${error ? 'has-error' : ''}`}>
      <label htmlFor={id}>
        {
          label
        }
      </label>
      <input
        className={`form-control ${className}`}
        id={id}
        placeholder={label}
        {...nativeProps}
      />
      {
        error ?
          <span className="help-block">
            {error}
          </span> :
          ''
      }
    </div>
  );
};

export default Input;
