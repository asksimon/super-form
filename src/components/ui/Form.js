import React from 'react';

const Form = props => {
  const {
    rules,
    values,
    children,
    cbError,
    cbSubmit,
    ...nativeProps,
  } = props;

  const validateData = () => {
    const rulesKeys = Object.keys(rules);
    const newErrorState = {};
    let formIsValid = true;

    rulesKeys.map(ruleKey => {
      const value = values[ruleKey];
      const rulesForField = rules[ruleKey];

      if (rulesForField.required && value === '') {
        newErrorState[ruleKey] = 'Поле обязательно для заполнения.';

        formIsValid = false;
      } else if (
        rulesForField.pattern &&
        value.match &&
        value.match(rulesForField.pattern) === null) {
        newErrorState[ruleKey] = 'Не верный формат строки.';

        formIsValid = false;
      } else if (rulesForField.isChecked && !value) {
        newErrorState[ruleKey] = 'Требуется потверждение.';

        formIsValid = false;
      } else {
        newErrorState[ruleKey] = null;
      }
    });

    return formIsValid || newErrorState;
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const validResult = validateData();

    validResult === true ? cbSubmit() : cbError(validResult);
  };

  return (
    <form onSubmit={handleSubmit} {...nativeProps}>
      {children}
    </form>
  );
};

export default Form;