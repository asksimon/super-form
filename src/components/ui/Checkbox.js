import React from 'react';

const Checkbox = props => {
  const {
    label,
    className,
    error,
    ...nativeProps
  } = props;

  return (
    <div className={`form-group ${error ? 'has-error' : ''}`}>
      <div className={`checkbox ${className}`}>
        <label>
          <input type="checkbox" {...nativeProps}/> {label}
        </label>
      </div>
      {
        error ?
          <span className="help-block">
            {error}
          </span> :
          ''
      }
    </div>
  );
};

export default Checkbox;
