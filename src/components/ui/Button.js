import React from 'react';

const Button = props => {
  const {
    className,
    children,
    ...nativeProps
  } = props;

  return (
    <button
      className={`btn btn-default ${className}`}
      {...nativeProps}
    >
      {children}
    </button>
  );
};

export default Button;
