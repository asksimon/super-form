import React from 'react';
import PropTypes from 'prop-types';

const NavBar = (props, context) => {
  const {
    changeCurrentForm
  } = props;

  const {
    currentForm
  } = context;

  return (
    <nav className="navbar navbar-default">
      <div className="container-fluid">
        <div className="navbar-header">
          <ul className="nav navbar-nav">
            <li
              className={currentForm === 'PersonalData' ? 'active' : ''}
              onClick={() => changeCurrentForm('PersonalData')}
            >
              <a>Личные данные</a>
            </li>
            <li
              className={currentForm === 'BankCard' ? 'active' : ''}
              onClick={() => changeCurrentForm('BankCard')}
            >
              <a>Номер банковской карты</a>
            </li>
            <li
              className={currentForm === 'Completion' ? 'active' : ''}
              onClick={() => changeCurrentForm('Completion')}
            >
              <a>Завершение</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

NavBar.contextTypes = {
  currentForm: PropTypes.string
};

export default NavBar;
