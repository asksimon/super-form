import React from 'react';

import Form from './forms';

const Content = () => {
  return (
    <div className="container">
      <Form />
    </div>
  )
};

export default Content;
