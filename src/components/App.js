import React, { Component } from 'react';
import PropTypes from 'prop-types';

import NavBar from './NavBar';
import Content from './Content';

class App extends Component {
  state = {
    currentForm: 'PersonalData'
  };

  formsList = [
    'PersonalData',
    'BankCard',
    'Completion'
  ];

  getChildContext = () => {
    return {
      currentForm: this.state.currentForm,
      goNextForm: () => {
        let indexCurrentForm = this.formsList.indexOf(this.state.currentForm);

        if (indexCurrentForm === this.formsList.length - 1) return;

        this.setState({
          currentForm: this.formsList[indexCurrentForm + 1]
        });
      },

      goPrevForm: () => {
        let indexCurrentForm = this.formsList.indexOf(this.state.currentForm);

        if (indexCurrentForm === 0) return;

        this.setState({
          currentForm: this.formsList[indexCurrentForm - 1]
        });
      }
    }
  };

  changeCurrentForm = type => {
    this.setState({
      currentForm: type
    })
  };

  render() {
    return (
      <div>
        <NavBar changeCurrentForm={this.changeCurrentForm} />
        <Content />
      </div>
    )
  }
}

App.childContextTypes = {
  currentForm: PropTypes.string,
  goNextForm: PropTypes.func,
  goPrevForm: PropTypes.func,
};

export default App;
