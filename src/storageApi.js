export const getData = (dataName = '') => {
  const promise = new Promise(resolve => {
    const data = localStorage.getItem(dataName);

    if (data) {
      resolve(JSON.parse(data));
    } else {
      resolve(null);
    }
  });

  return promise;
};

export const setData = (dataName = '', data = {}) => {
  const promise = new Promise(resolve => {
    localStorage.setItem(dataName, JSON.stringify(data));

    resolve(true);
  });

  return promise;
};
