import { combineReducers } from 'redux';

import bankCard from './bankCard';
import personalData from './personalData';

export default combineReducers({
  bankCard,
  personalData
});
