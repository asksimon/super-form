import {SET_DATA} from '@actions/bankCard';

export default (state = null, action) => {
  switch(action.type) {
    case SET_DATA:
      return state = Object.assign({}, state, action.data);
    default:
      return state;
  }
}
