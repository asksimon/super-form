import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from './store';

import App from './components/App';

const el = document.getElementById('app');

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, el);
