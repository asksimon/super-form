export const SET_DATA = 'SET_DATA_BANK_CARD';

export const setData = (data) => {
  return {
    type: SET_DATA,
    data
  };
};
